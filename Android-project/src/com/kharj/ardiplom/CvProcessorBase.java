package com.kharj.ardiplom;

import java.util.ArrayList;

import org.opencv.core.*;
import org.opencv.imgproc.Imgproc;

import android.content.Context;

public class CvProcessorBase {
	protected boolean isShowOverlay = true;
	protected Mat frame = null;
	protected Context appContext;
	protected String stringData = "";
	
	
	public void Init(Context _context){
		this.appContext = _context;
	}
	public void Destroy(){
		frame = null;
	}
	public void onResume(){
		frame = null;		
	}
	public void onPause(){
		frame = null;
		
	}
	public boolean isShowOverlay() {
		return isShowOverlay;
	}
	public void setShowOverlay(boolean isShowOverlay) {
		this.isShowOverlay = isShowOverlay;
	}
	public boolean pushFrame(Mat _frame){
		frame = _frame;
		process();
		return true;
	}
	public Mat pullFrame(){
		return frame;		
	}
	protected void process(){
		if(frame == null) return;
		//Process
		
		if(isShowOverlay){
			//Draw
			Core.rectangle(frame, new Point(0, 0), new Point(frame.width()/2, frame.height()/2), new Scalar(255, 255, 0), 25);
			//Imgproc.GaussianBlur(frame, frame, new Size(3,3), 2);
			//cvNativeProcessBase(frame.getNativeObjAddr());
			
		}
	}

	public String pullStringData(){
		return stringData;		
	}
	protected void pushStringData(String data){
	
	}
	
	public static native void cvNativeProcessBase(long matAddrRgba);
}
