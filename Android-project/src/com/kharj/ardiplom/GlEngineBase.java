package com.kharj.ardiplom;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.Log;

public class GlEngineBase {


	protected boolean isShowOverlay = true;
	protected GLSurfaceView surfaceView;
	protected AREngineBase engine;
	protected Context appContext;
	protected GlRendererBase renderer;
	protected int lastFps;

	public boolean Init(Context _context, AREngineBase _engine, GLSurfaceView _surfaceView){
		if(_context == null || _surfaceView == null) 
			return false;
		this.engine = _engine;
		this.appContext = _context;
		this.surfaceView = _surfaceView;

		renderer = new GlRendererBlank(appContext);
		if(renderer == null)
			return false;

		surfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
		surfaceView.setEGLContextClientVersion(2);
		surfaceView.setEGLConfigChooser( 8, 8, 8, 8, 16, 0 );
		surfaceView.setPreserveEGLContextOnPause(true);
		surfaceView.setRenderer(renderer);		
		return true;
	}

	public void Destroy(){

	}
	public void onResume(){
		if (surfaceView != null) {
			surfaceView.onResume();
		}
	}
	public void onPause(){
		if (surfaceView != null) {
			surfaceView.onPause();
		}
	}
	public boolean isShowOverlay() {
		return isShowOverlay;
	}

	public void setShowOverlay(boolean _isShowOverlay) {
		this.isShowOverlay = _isShowOverlay;
		if(renderer != null){
			surfaceView.queueEvent(new Runnable() {
				public void run() {
					renderer.setShowOverlay(isShowOverlay);
				}});
		}
	}
	public int getFPS() {
		if(renderer != null){
			surfaceView.queueEvent(new Runnable() {
				public void run() {
					lastFps = renderer.getFPS();
				}});
		}
		return lastFps;
	}
	public void onMessage(String code, String message) {

	}
}
