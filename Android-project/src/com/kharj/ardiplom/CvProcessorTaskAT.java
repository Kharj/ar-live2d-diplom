package com.kharj.ardiplom;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.os.AsyncTask;
import android.util.Log;

public class CvProcessorTaskAT extends CvProcessorTaskBase {
	protected boolean isBusy = false;
	protected MyTask myTask = null;

	@Override
	public void clean(){
		frame = null;
		startTime = 0;
		myTask = null;
	}
	@Override
	public boolean start(Mat _frame){
		if(isBusy() || myTask == null) return false;
		frame = _frame;

		try{
			myTask = new MyTask();
			myTask.execute(frame);
		}catch(IllegalThreadStateException e){
			Log.i(AREngineBase.TAG, "Can't start async task");
			myTask = null;
			return false;
		}
		startTime = System.currentTimeMillis();

		Log.i(AREngineBase.TAG, "Async Task notifyed");

		return true;

	}
	@Override
	public void stop(){
		if(myTask != null){
			myTask.cancel(true);
		}
		clean();
	}
	@Override
	public boolean isBusy(){

		return isBusy;
	}

	class MyTask extends AsyncTask<Mat, Void, Void> {
		protected Mat rgb = null;
		protected Void doInBackground(Mat... params) {
			if(rgb != null){
				//work
				Core.rectangle(rgb, new Point(0, 0), new Point(frame.width()/2, frame.height()/2), new Scalar(255, 255, 0));
				Imgproc.GaussianBlur(rgb, rgb, new Size(3,3), 2);
				//CvProcessorMTBase.cvNativeProcessBase(frame.getNativeObjAddr());					
			}
			return null;
		}

		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			isBusy = true;
			super.onPreExecute();
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			isBusy = false;
		}

	}
}

