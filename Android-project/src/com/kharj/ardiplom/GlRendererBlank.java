package com.kharj.ardiplom;

import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLES20;

public class GlRendererBlank extends GlRendererBase {

	public GlRendererBlank(Context _appContext){
		super(_appContext);
	
	}
	@Override
	public void onCreate(int width, int height,
			boolean contextLost) {
		//GLES20.glClearColor(0f, 0f, 1f, 0.001f);
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		super.onDrawFrame(gl);
		if(!isShowOverlay){
			GLES20.glClearColor(0f, 0f, 0f, 0f);
		}else{
			GLES20.glClearColor(0f, 0f, 0f, 0.001f);
		}
		GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT  | GLES20.GL_DEPTH_BUFFER_BIT);
	}

}
