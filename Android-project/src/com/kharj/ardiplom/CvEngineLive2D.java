package com.kharj.ardiplom;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.objdetect.CascadeClassifier;

import com.kharj.ardiplom.AREngineBase.MSG_ENUM;

import android.app.Service;
import android.content.Context;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.WindowManager;

public class CvEngineLive2D extends CvEngineBase {

	protected boolean isShowImage = true;
	protected int mode = 0;

	@Override
	public boolean Init(Context context, AREngineBase _engine, CameraBridgeViewBase _cameraView){
		if(context == null || _cameraView == null) 
			return false;
		fpsLastTime = System.currentTimeMillis();
		fpsFrames = 0;
		this.engine = _engine;
		this.cameraView = _cameraView;
		this.appContext = context;
		this.loaderCallback = new BaseLoaderCallback(appContext) {
			@Override
			public void onManagerConnected(int status) {
				switch (status) {
				case LoaderCallbackInterface.SUCCESS:
				{
					Log.i(AREngineBase.TAG, "OpenCV loaded successfully");
					//Lib
					System.loadLibrary("ar-diplom-native");

					//Processor
					createProcessor();
					if(processor != null){
						processor.Init(appContext);
						((CvProcessorFaceDetection)processor).setMode(mode);
					}

					cameraView.enableView();
				} break;
				default:
				{
					super.onManagerConnected(status);
				} break;
				}
			}
		};
		if(loaderCallback == null) 
			return false;
		cameraView.setVisibility(SurfaceView.VISIBLE);
		cameraView.setCvCameraViewListener(this);

		return true;
	}

	@Override
	protected void createProcessor(){
		processor = new CvProcessorFaceDetection();
	}

	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		fpsWork();
		Mat source = getMatFromFrameWithRotation(inputFrame, true, flip);
		frameWidth = source.width();
		frameHeight = source.height();
		if(processor != null){
			processor.pushFrame(source);
			source = processor.pullFrame();
			String faceData = processor.pullStringData();
			engine.sendMessage(MSG_ENUM.TO_GL, "FaceData", faceData);
		}
		if(!isShowImage){
			source.setTo(new Scalar(0, 0, 0));
		}
		return source;
	}
	@Override
	public void onMessage(String code, String message) {
		super.onMessage(code, message);
		Log.i(AREngineBase.TAG, "msg to cv"+message);
		if(code == "showImage"){
			isShowImage = ((message=="1") ? true : false);
		}else if(code == "smileTreshold"){
			try{
				int intValue = Integer.parseInt(message);
				((CvProcessorFaceDetection)processor).setSmileTreshold(((float)intValue)*0.01f);
			}catch(NumberFormatException ex){

			}
		}else if(code == "mode"){
			this.mode = Integer.parseInt(message);
			if(processor != null){
				((CvProcessorFaceDetection)processor).setMode(this.mode);
			}
		}
		
	}

}
