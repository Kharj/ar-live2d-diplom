package com.kharj.ardiplom;

import java.io.IOException;
import java.io.InputStream;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import jp.live2d.Live2D;
import jp.live2d.android.Live2DModelAndroid;
import jp.live2d.android.UtOpenGL;
import jp.live2d.util.UtSystem;

public class GlRendererLive2D extends GlRendererBase {
	protected float faceX, faceY;
	protected float lookX, lookY;
	protected float mouthForm, mouthOpen;
	protected float mouthFormWanted, mouthOpenWanted;
	protected float mouthStep = 0.04f;
	protected float headAngle;
	protected float eyeForm;
	protected float aspect = 1.0f;
	private Live2DModelAndroid	live2DModel ;
	private final String MODEL_PATH = "haru/haru.moc" ;
	private final String TEXTURE_PATHS[] =
		{
			"haru/haru.1024/texture_00.png" ,
			"haru/haru.1024/texture_01.png" ,
			"haru/haru.1024/texture_02.png"
		} ;

	public GlRendererLive2D(Context _appContext){
		super(_appContext);
		Live2D.init();

	}

	@Override
	public void onDrawFrame(GL10 gl)
	{
		super.onDrawFrame(gl);

		gl.glClearColor(0f, 0f, 0f, 0.001f);
		gl.glClear( GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT ) ;

		double t = (UtSystem.getUserTimeMSec()/1000.0) * 2 * Math.PI  ;
		double cycle=3.0;
		double sin=Math.sin( t/cycle );
		if(Math.abs(mouthForm-mouthFormWanted)>0.9f*mouthStep){
			mouthForm += ((mouthForm<mouthFormWanted)?mouthStep:(-mouthStep));
		}
		if(Math.abs(mouthOpen-mouthOpenWanted)>0.9f*mouthStep){
			mouthOpen += ((mouthOpen<mouthOpenWanted)?mouthStep:(-mouthStep));
		}
		
		
		float modelWidth = live2DModel.getCanvasWidth();
		gl.glMatrixMode(GL10.GL_MODELVIEW ) ;
		gl.glLoadIdentity() ;
		gl.glTranslatef(faceX*modelWidth, faceY*modelWidth/aspect, 0.0f);

		
		live2DModel.setParamFloat( "PARAM_MOUTH_FORM" , mouthForm ) ;
		live2DModel.setParamFloat( "PARAM_MOUTH_OPEN_Y" , mouthOpen ) ;
		live2DModel.setParamFloat( "PARAM_ANGLE_Z" , headAngle ) ;
	
		live2DModel.setParamFloat( "PARAM_EYE_BALL_X" , lookX) ;
		live2DModel.setParamFloat( "PARAM_EYE_BALL_Y" , lookY ) ;
		live2DModel.setParamFloat( "PARAM_ANGLE_X" , lookX*10.0f ) ;
		live2DModel.setParamFloat( "PARAM_ANGLE_Y" , lookY*3.4f) ;
		live2DModel.setParamFloat( "PARAM_EYE_FORM" , eyeForm) ;
		live2DModel.setParamFloat( "PARAM_BREATH" , (float)sin * 0.22f) ;

		live2DModel.setGL( gl ) ;
		live2DModel.update() ;
		if(isShowOverlay){
			live2DModel.draw() ;
		}
	}


	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height)
	{

		gl.glViewport( 0 , 0 , width , height ) ;


		gl.glMatrixMode( GL10.GL_PROJECTION ) ;
		gl.glLoadIdentity() ;

		float modelWidth = live2DModel.getCanvasWidth();
		aspect = (float)width/height;
		

		gl.glOrthof(0 ,	modelWidth , modelWidth / aspect , 0 , 0.5f , -0.5f ) ;
	}


	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config)
	{

		try
		{
			InputStream in = appContext.getAssets().open( MODEL_PATH ) ;
			live2DModel = Live2DModelAndroid.loadModel( in ) ;
			in.close() ;

			for (int i = 0 ; i < TEXTURE_PATHS.length ; i++ )
			{
				InputStream tin = appContext.getAssets().open( TEXTURE_PATHS[i] ) ;
				int texNo = UtOpenGL.loadTexture(gl , tin , true ) ;
				live2DModel.setTexture( i , texNo ) ;
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	public void setFaceTranslate(float _x, float _y){
		faceX = _x;
		faceY = _y;
	}
	public void setMouthParams(float _form, float _open){
		mouthFormWanted = _form;
		mouthOpenWanted = _open;
	}
	public void setLook(float _x, float _y){
		lookX = _x;
		lookY = _y;
	}
	public void setHeadAngle(float _angle){
		headAngle = _angle;
	}
	public void setEyeForm(float _form){
		eyeForm = _form;
	}
}
