package com.kharj.ardiplom;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.core.Mat;

import android.util.Log;

public class CvEngineMTBase extends CvEngineBase {
	private long fpsProcFrames;
	private int fpsProcValue;
	@Override
	public void Destroy(){
		super.Destroy();
	}

	@Override
	public void onResume(){
		super.onResume();
	}

	@Override
	public void onPause(){
		super.onPause();
	}

	@Override
	public void onCameraViewStarted(int width, int height) {

	}

	@Override
	public void onCameraViewStopped() {

	}
	@Override
	public int getProcessorFPS() {
		return fpsProcValue;
	}
	@Override
	protected void fpsWork(){
		//FPS
		fpsFrames++;
		long currentTime = System.currentTimeMillis();
		if (currentTime - fpsLastTime >= 1000) {
			fpsValue = (int)((float)(fpsFrames*1000)/(currentTime - fpsLastTime) + 0.5f);
			fpsFrames = 0;
			fpsProcValue = (int)((float)(fpsProcFrames*1000*100)/(currentTime - fpsLastTime) + 0.5f);
			fpsProcFrames = 0;
			fpsLastTime = currentTime;
		}

	}
	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		fpsWork();
		Mat source = inputFrame.rgba();	
		Mat res = processor.pullFrame();
		if(res == null){
			//if any processed frame exists, show it
			//else, sho wsource frame
			res = source;
		}else{
			fpsProcFrames++;
		}
		processor.pushFrame(source);
		return res;
	}

	@Override
	protected void createProcessor() {
		processor = new CvProcessorMTBase();
		//processor = new CvProcessorAsyncBase();
	}


}
