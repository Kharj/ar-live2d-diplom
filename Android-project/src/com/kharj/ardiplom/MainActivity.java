package com.kharj.ardiplom;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import com.kharj.ardiplom.AREngineBase.MSG_ENUM;
import com.kharj.arlive2ddiplom.R;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup.LayoutParams;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnTouchListener {
	private AREngineBase arEngine;
	private GLSurfaceView glSurfaceView;
	private CameraBridgeViewBase cvCameraView;
	private TextView fpsTextView;
	private Thread fpsTextThread;
	final private String SAVED_CAMERA_ID = "saved_camera_id";
	final private String SAVED_LIVE2D_MODE_ID = "saved_live2d_mode_id";
	final private String SAVED_CAMERA_FLIP = "saved_flip_id";
	private int smileTreshold = 50;
	private int live2d_mode = 0;
	private boolean camera_flip = false;
	final private int live2d_mode_max = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Camera select 
		SharedPreferences sPreferences = getPreferences(MODE_PRIVATE);
		int cameraId = sPreferences.getInt(SAVED_CAMERA_ID, 0);
		if(cameraId+1 > Camera.getNumberOfCameras()){
			cameraId = 0;
		}
		live2d_mode = sPreferences.getInt(SAVED_LIVE2D_MODE_ID, 0);
		if(live2d_mode > live2d_mode_max){
			live2d_mode = 0;
		}
		camera_flip = sPreferences.getBoolean(SAVED_CAMERA_FLIP, false);
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		//Layout
		int textHeight = 100;
		setContentView(R.layout.activity_main);
		//cvCameraView = (CameraBridgeViewBase) findViewById(R.id.cvCameraView1);
		//glSurfaceView = (GLSurfaceView) findViewById(R.id.glSurfaceView1);
		cvCameraView = new JavaCameraView(this, cameraId);
		glSurfaceView = new GLSurfaceView(this);
		fpsTextView = new TextView(this);
		fpsTextView.setTextColor(Color.RED);
		View mainView = (View)findViewById(R.id.activityMainView);
		mainView.setOnTouchListener(this);
		//Engine
		//arEngine = new AREngineBase();
		arEngine = new AREngineAnime();
		if(arEngine.Init(this, cvCameraView, glSurfaceView)){
			addContentView(cvCameraView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			addContentView(glSurfaceView, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
			addContentView(fpsTextView, new LayoutParams(LayoutParams.MATCH_PARENT, textHeight));
			glSurfaceView.setZOrderMediaOverlay(true);
			arEngine.sendMessage(MSG_ENUM.TO_CV, "mode", String.valueOf(live2d_mode));
			arEngine.sendMessage(MSG_ENUM.TO_CV, "flip", String.valueOf(camera_flip));
			arEngine.sendMessage(MSG_ENUM.TO_CV, "cameraId", String.valueOf(cameraId));
		}
		//FPS
		startFPSTimer();
	}
	@Override
	protected void onResume() {
		super.onResume();
		arEngine.onResume();
		startFPSTimer();

	}

	@Override
	protected void onPause() {
		super.onPause();
		arEngine.onPause();
		if(fpsTextThread != null){
			fpsTextThread.interrupt();
			fpsTextThread = null;
		}
	}

	public void onDestroy() {
		super.onDestroy();
		arEngine.Destroy();
		if(fpsTextThread != null){
			fpsTextThread.interrupt();
			fpsTextThread = null;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);

		 MenuItem modeItem = menu.findItem(R.id.action_live2d_mode);
		 modeItem.setTitle((live2d_mode==0)?"Mode copy":"Mode track");
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_about) {
			Toast.makeText(getBaseContext(), "не дипломная работа\nevgen.kharchenko.ua@gmail.com\nbitbucket.org/Kharj/ar-live2d-diplom", Toast.LENGTH_LONG).show();
			return true;
		}else if(id == R.id.action_2DOverlay){
			item.setChecked(!item.isChecked());
			arEngine.setShowCVOverlay(item.isChecked());
			return true;
		}else if(id == R.id.action_3DOverlay){
			item.setChecked(!item.isChecked());
			arEngine.setShowGLOverlay(item.isChecked());
			return true;
		}else if(id == R.id.action_showFPS){
			item.setChecked(!item.isChecked());
			arEngine.setShowFPS(item.isChecked());
			fpsTextView.setVisibility((!item.isChecked())?View.INVISIBLE:View.VISIBLE);
			return true;
		}else if(id == R.id.action_swap_camera){
			SharedPreferences sPreferences = getPreferences(MODE_PRIVATE);
			int cameraId = sPreferences.getInt(SAVED_CAMERA_ID, 0);
			cameraId = (cameraId==0) ? 1 : 0;
			Editor ed = sPreferences.edit();
			ed.putInt(SAVED_CAMERA_ID, cameraId);
			ed.commit();
			Toast.makeText(getBaseContext(), "Restart app to change camera.", Toast.LENGTH_SHORT).show();
			return true;
		}else if(id == R.id.action_smile_treshold){
			smileTreshold += 10;
			if(smileTreshold > 110){//110 = never
				smileTreshold = 10;
			}
			arEngine.sendMessage(MSG_ENUM.TO_CV, "smileTreshold", Integer.toString(smileTreshold));
			item.setTitle("smile treshold "+((smileTreshold>100)?"never":smileTreshold)+"%");
			return true;
		}else if(id == R.id.action_showCamera){
			item.setChecked(!item.isChecked());
			arEngine.sendMessage(MSG_ENUM.TO_CV, "showImage", (item.isChecked())?"1":"0");
			return true;
		}else if(id == R.id.action_live2d_mode){
			live2d_mode ++;
			if(live2d_mode > live2d_mode_max){
				live2d_mode = 0;
			}
			item.setTitle((live2d_mode==0)?"Mode copy":"Mode track");
			SharedPreferences sPreferences = getPreferences(MODE_PRIVATE);
			Editor ed = sPreferences.edit();
			ed.putInt(SAVED_LIVE2D_MODE_ID, live2d_mode);
			ed.commit();
			arEngine.sendMessage(MSG_ENUM.TO_CV, "mode", String.valueOf(live2d_mode));
			return true;
		}else if(id == R.id.action_flipCamera){
			item.setChecked(!item.isChecked());
			camera_flip = item.isChecked();
			SharedPreferences sPreferences = getPreferences(MODE_PRIVATE);
			Editor ed = sPreferences.edit();
			ed.putBoolean(SAVED_CAMERA_FLIP, camera_flip);
			ed.commit();
			arEngine.sendMessage(MSG_ENUM.TO_CV, "flip", String.valueOf(camera_flip));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void startFPSTimer(){
		fpsTextThread = new Thread() {
			@Override
			public void run() {
				try {
					while (!isInterrupted()) {
						Thread.sleep(1800);
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								if(arEngine != null){
									fpsTextView.setText(arEngine.getFPSString());
								}
							}
						});
					}
				} catch (InterruptedException e) {
				}
			}
		};

		fpsTextThread.start();
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		Log.i(AREngineBase.TAG, "touch: "+event.getX()+" "+event.getY());
		openOptionsMenu();
		return false;
	}
	
}
