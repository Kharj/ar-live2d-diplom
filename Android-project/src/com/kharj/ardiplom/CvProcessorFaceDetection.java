package com.kharj.ardiplom;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringBufferInputStream;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

public class CvProcessorFaceDetection extends CvProcessorBase{

	protected CascadeClassifier faceDetector = null;
	protected CascadeClassifier smileDetector = null;
	protected long faceNativeObject = 0;
	protected long smileNativeObject = 0;
	protected long eyeNativeObject = 0;
	protected Mat grayMap = null;
	protected Mat grayMapBig = null;
	protected int grayWidth, grayHeight;
	protected int statCounter = 0;
	protected int smileAccumulator = 0;
	private int mAbsoluteFaceSize   = 0;
	private int mode = 0;
	//Filenames
	protected String cascadeDirName = "cascades";
	//protected String cascadeFileName = "lbpcascade_frontalface.xml";
	protected String cascadeFileNameSmile = "haarcascade_smile.xml";
	protected String cascadeFileNameEye = "haarcascade_eye.xml";
	protected String cascadeFileNameFace = "haarcascade_frontalface_alt.xml";
	//Settings
	protected int statLimit = 20;//need to connect with fps
	protected float smileTreshold = 0.5f;
	protected float graySizeMultiplier = 0.5f;
	private float mRelativeFaceSize   = 0.18f;


	public void setSmileTreshold(float smileTreshold) {
		this.smileTreshold = smileTreshold;
		Log.i(AREngineBase.TAG, "tresh "+smileTreshold);
	}

	@Override
	public void Init(Context _context){
		super.Init(_context);
		//Cascade detector
		// load cascade file from application resources
		//FACE DETECTOR
		faceNativeObject = createDetectorNativeObject(cascadeDirName, cascadeFileNameFace);
		//SMILE DETECTOR
		smileNativeObject = createDetectorNativeObject(cascadeDirName, cascadeFileNameSmile);
		//SMILE DETECTOR
		eyeNativeObject = createDetectorNativeObject(cascadeDirName, cascadeFileNameEye);
	}
	protected long createDetectorNativeObject(String dir, String filename){
		long res = 0;
		try{
			File mCascadeFile = null;
			InputStream is = appContext.getAssets().open(dir+"/"+filename);
			File cascadeDir = appContext.getDir(dir, Context.MODE_PRIVATE);
			mCascadeFile = new File(cascadeDir, filename);
			FileOutputStream os = new FileOutputStream(mCascadeFile);

			byte[] buffer = new byte[4096];
			int bytesRead;
			while ((bytesRead = is.read(buffer)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			is.close();
			os.close();

			/*faceDetector = new CascadeClassifier(mCascadeFile.getAbsolutePath());
			if (faceDetector.empty()) {
				Log.e(AREngineBase.TAG, "Failed to load cascade classifier");
				faceDetector = null;
			} else{
				Log.i(AREngineBase.TAG, "Loaded cascade classifier from " + mCascadeFile.getAbsolutePath());
			}*/

			res = nativeCreateObject(mCascadeFile.getAbsolutePath(), 0);
			cascadeDir.delete();
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(AREngineBase.TAG, "Failed to load cascade. Exception thrown: " + e);
		}
		return res;
	}

	@Override
	public void Destroy() {
		//Native
		nativeDestroyObject(faceNativeObject);
		faceNativeObject = 0;
		nativeDestroyObject(smileNativeObject);
		smileNativeObject = 0;
		nativeDestroyObject(eyeNativeObject);
		eyeNativeObject = 0;

		super.Destroy();
	}

	@Override
	public boolean pushFrame(Mat _frame) {
		// TODO Auto-generated method stub
		return super.pushFrame(_frame);
	}

	@Override
	public Mat pullFrame() {
		// TODO Auto-generated method stub
		return super.pullFrame();
	}

	@Override
	protected void process() {
		if(grayMap==null){
			grayWidth = Math.round(frame.width()*graySizeMultiplier);
			grayHeight = Math.round(frame.height()*graySizeMultiplier);
			grayMap = new Mat(grayWidth, grayHeight, CvType.CV_8U);
			grayMapBig = new Mat(frame.width(), frame.height(), CvType.CV_8U);
		}
		if(frame == null || /*faceDetector==null*/ faceNativeObject == 0) return;
		//Process
		Imgproc.cvtColor(frame, grayMapBig, Imgproc.COLOR_BGR2GRAY);
		Imgproc.resize(grayMapBig, grayMap, new Size(grayWidth, grayHeight));
		if (mAbsoluteFaceSize == 0) {
			int height = grayMap.rows();
			if (Math.round(height * mRelativeFaceSize) > 0) {
				mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
			}
		}

		MatOfRect faces = new MatOfRect();
		MatOfRect smiles = new MatOfRect();
		MatOfRect eyes = new MatOfRect();

		/*if (faceDetector != null)
			faceDetector.detectMultiScale(grayMap, faces, 1.25, 3,
					Objdetect.CASCADE_FIND_BIGGEST_OBJECT |
					Objdetect.CASCADE_SCALE_IMAGE, 
					new Size(mAbsoluteFaceSize, mAbsoluteFaceSize), new Size());
		 */
		//nativeDetect(faceNativeObject, grayMap.getNativeObjAddr(), faces.getNativeObjAddr(), mAbsoluteFaceSize, mAbsoluteFaceSize);
		if(mode == 0){//tracking mode excludes eyes detection
			nativeDetectFaceSmile(faceNativeObject, smileNativeObject, eyeNativeObject, grayMap.getNativeObjAddr(), faces.getNativeObjAddr(), smiles.getNativeObjAddr(), eyes.getNativeObjAddr(), mAbsoluteFaceSize, mAbsoluteFaceSize);
		}else{
			nativeDetectFaceSmile(faceNativeObject, smileNativeObject, 0, grayMap.getNativeObjAddr(), faces.getNativeObjAddr(), smiles.getNativeObjAddr(), eyes.getNativeObjAddr(), mAbsoluteFaceSize, mAbsoluteFaceSize);
		}
		Rect[] facesArray = faces.toArray();
		Rect[] smilesArray = smiles.toArray();
		Rect[] eyesArray = eyes.toArray();
		stringData = "Face 0 0";
		for (int i = 0; i < facesArray.length; i++){
			Rect rect = facesArray[i];
			scaleRect(rect, (1.0f/graySizeMultiplier));
			float faceCenterX = 1000.0f*((float)rect.x+rect.width*0.5f)/(float)frame.width();
			float faceCenterY = 1000.0f*((float)rect.y+rect.height*0.5f)/(float)frame.height();
			stringData = "Face "+((int)faceCenterX)+" "+((int)faceCenterY);

			if(isShowOverlay){
				Core.rectangle(frame, rect.tl(), rect.br(), new Scalar(0, 255, 0), 2);
			}

		}
		//Smile process
		int isSmile = 0;
		for (int i = 0; i < smilesArray.length; i++){
			Rect rect = smilesArray[i];
			scaleRect(rect, (1.0f/graySizeMultiplier));
			isSmile = 1;
			if(isShowOverlay){
				Core.rectangle(frame, rect.tl(), rect.br(), new Scalar(100, 0, 255), 2);
			}

		}
		//Stats
		if(statCounter == statLimit){
			statCounter = 0;
			smileAccumulator = 0;
		}
		statCounter++;
		smileAccumulator += isSmile;
		float smileValue = (float)smileAccumulator/(float)(statCounter);
		isSmile = (smileValue >= smileTreshold) ? 1 : 0;
		stringData += " " + isSmile;
		//Eyes
		Point eyeCenter1 = null;
		Point eyeCenter2 = null;
		for (int i = 0; i < eyesArray.length; i++){
			Rect rect = eyesArray[i];
			scaleRect(rect, (1.0f/graySizeMultiplier));
			if(eyesArray.length == 2){
				if(i==0) eyeCenter1 = new Point(rect.x+rect.width*0.5f, rect.y+rect.height*0.5f);
				if(i==1) eyeCenter2 = new Point(rect.x+rect.width*0.5f, rect.y+rect.height*0.5f);
			}
			if(isShowOverlay){
				Core.rectangle(frame, rect.tl(), rect.br(), new Scalar(255, 0, 50), 2);
			}
		}
		float angle = 0.0f;
		if(eyeCenter1 != null && eyeCenter2 != null){
			if(eyeCenter2.x < eyeCenter1.x){
				Point t = eyeCenter1;
				eyeCenter1 = eyeCenter2;
				eyeCenter2 = t;
			}
			Point centerPoint = new Point((eyeCenter1.x+eyeCenter2.x)*0.5f, (eyeCenter1.y+eyeCenter2.y)*0.5f);
			angle = (float)Math.atan2(Math.abs(eyeCenter1.y - centerPoint.y), Math.abs(eyeCenter1.x - centerPoint.x));
			angle *= 90.0f*3.14f;
			if(eyeCenter1.y > eyeCenter2.y){
				angle = -angle;
			}
			if(Math.abs(angle) > 45.0f){
				angle = 0;
			}
			Log.i(AREngineBase.TAG, "Angle: "+angle);
		}
		stringData += " " + (int)(angle*1000.0f);
		stringData += " " + facesArray.length;
		stringData += " " + mode;
	}		
	protected void scaleRect (Rect r, float mul) {
		r.x = Math.round(r.x * mul);
		r.y = Math.round(r.y * mul);
		r.width = Math.round(r.width * mul);
		r.height = Math.round(r.height * mul);
	}

	@Override
	public String pullStringData() {
		return super.pullStringData();
	}

	@Override
	protected void pushStringData(String data) {

	}
	protected void setMode(int m){
		this.mode = m;
	}

	private static native long nativeCreateObject(String cascadeName, int minFaceSize);
	private static native void nativeDestroyObject(long thiz);
	private static native void nativeDetect(long thiz, long inputImage, long faces, int minSizeX, int minSizeY);

	// private static native void nativeDetectFaceSmile(long thiz, long thiz2, long inputImage, long faces, long smiles, int minSizeX, int minSizeY);
	private static native void nativeDetectFaceSmile(long thiz, long thiz2, long thiz3, long inputImage, long faces, long smiles, long eyes, int minSizeX, int minSizeY);
}



