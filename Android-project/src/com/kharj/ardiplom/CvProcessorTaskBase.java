package com.kharj.ardiplom;
import  org.opencv.core.*;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class CvProcessorTaskBase {
	protected Mat frame; //isEmpty
	protected long startTime;
	protected CvProcessorThread thread;
	protected Handler handler;

	public void clean(){
		frame = null;
		startTime = 0;
	}
	public boolean start(Mat _frame){
		frame = _frame;
		try{
			if(isBusy() || _frame == null) return false;

			if(thread == null){
				//create thread
				thread = new CvProcessorThread();
			}
			try{
				thread.start();
			}catch(IllegalThreadStateException e){
				Log.i(AREngineBase.TAG, "Can't start thread");
				thread = null;
				return false;
			}
			startTime = System.currentTimeMillis();
			synchronized (thread) {
				//thread.wait();
				thread.frame = _frame;
				thread.notifyAll();	
			}
			Log.i(AREngineBase.TAG, "Thread notifyed");
		}catch(Exception e){
			e.printStackTrace();
		}
		return true;

	}
	public void stop(){
		if(thread != null){
			synchronized (thread) {
			thread.interrupt();
			thread = null;
			}
		}
		clean();
	}
	public boolean isBusy(){
		if (thread != null  && thread.getState() == Thread.State.RUNNABLE){
			return true;
		}
		return false;
	}
	public boolean isEmpty(){
		return (frame == null);
	}
	public long getStartTime() {
		return startTime;
	}


}
