package com.kharj.ardiplom;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLSurfaceView.Renderer;
import android.util.Log;

public class GlRendererBase implements Renderer {

	private boolean mFirstDraw;
	private boolean mSurfaceCreated;
	private int mWidth;
	private int mHeight;
	private long fpsLastTime;
	private long fpsFrames;
	private int fpsValue;
	protected boolean isShowOverlay = true;
	protected Context appContext;

	public GlRendererBase(Context _appContext) {
		appContext = _appContext;
		mFirstDraw = true;
		mSurfaceCreated = false;
		mWidth = -1;
		mHeight = -1;
		fpsLastTime = System.currentTimeMillis();
		fpsFrames = 0;
	}

	@Override
	public void onDrawFrame(GL10 gl) {
		fpsWork();
	}

	@Override
	public void onSurfaceChanged(GL10 gl, int width, int height) {
		if (!mSurfaceCreated && width == mWidth && height == mHeight) {
			//Log.i(TAG, "Surface changed but already handled.");
			return;
		}
		// Android honeycomb has an option to keep the
		// context.
		String msg = "Surface changed width:" + width + " height:" + height;
		if (mSurfaceCreated) {
			msg += " context lost.";
		} else {
			msg += ".";
		}
		Log.i(AREngineBase.TAG, msg);

		mWidth = width;
		mHeight = height;

		onCreate(mWidth, mHeight, mSurfaceCreated);
		mSurfaceCreated = false;
	}

	@Override
	public void onSurfaceCreated(GL10 gl, EGLConfig config) {
		mSurfaceCreated = true;
		mWidth = -1;
		mHeight = -1;
	}

	public int getFPS() {
		return fpsValue;
	}

	public void setShowOverlay(boolean isShowOverlay) {
		this.isShowOverlay = isShowOverlay;
	}

	protected void fpsWork(){
		//FPS
		fpsFrames++;
		long currentTime = System.currentTimeMillis();
		if (currentTime - fpsLastTime >= 1000) {
			fpsValue = (int)((float)(fpsFrames*1000)/(currentTime - fpsLastTime) + 0.5f);
			fpsFrames = 0;
			fpsLastTime = currentTime;
		}
	}
	public void onCreate(int width, int height,
			boolean contextLost){
		
	}


}
