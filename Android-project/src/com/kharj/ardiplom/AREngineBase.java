package com.kharj.ardiplom;

import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;

import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.Log;

public class AREngineBase {
	//static
	public enum MSG_ENUM { TO_GL, TO_CV};
	public final static String TAG = "AREngine";
	//public
	public CvEngineBase cvEngine;
	public GlEngineBase glEngine;
	
	//private
	private boolean isShowFPS = true;
	
	public CvEngineBase GetCvEngine(){
		return cvEngine;
	}
	public GlEngineBase GetGlEngine(){
		return glEngine;
	}
	
	public boolean Init(Context context, CameraBridgeViewBase cvCameraView, GLSurfaceView glSurfaceView){
		createEngines();	
		if(cvEngine == null || glEngine == null)
			return false;
		boolean res = cvEngine.Init(context, this, cvCameraView);
		if(res) Log.i(AREngineBase.TAG, "CV Engine loaded");
		if(!res) 
			return res;
		res = glEngine.Init(context, this, glSurfaceView);
		if(res) Log.i(AREngineBase.TAG, "GL Engine loaded");
		return res;
	}
	
	public void Destroy(){
		cvEngine.Destroy();
		glEngine.Destroy();
	}
	
	public void onResume(){
		cvEngine.onResume();
		glEngine.onResume();
	}
	public void onPause(){
		cvEngine.onPause();
		glEngine.onPause();
	}
	public boolean isShowCVOverlay() {
		return cvEngine.isShowOverlay();
	}
	public void setShowCVOverlay(boolean isShowCVOverlay) {
		cvEngine.setShowOverlay(isShowCVOverlay);
	}
	public boolean isShowGLOverlay() {
		return glEngine.isShowOverlay();
	}
	public void setShowGLOverlay(boolean isShowGLOverlay) {
		glEngine.setShowOverlay(isShowGLOverlay);
	}
	public boolean isShowFPS() {
		return isShowFPS;
	}
	public void setShowFPS(boolean isShowFPS) {
		this.isShowFPS = isShowFPS;
	}
	
	public String getFPSString(){
		return String.format("cam fps: %d. cv fps: %d. gl fps: %d. %dx%d", cvEngine.getFPS(), cvEngine.getProcessorFPS(), glEngine.getFPS(), cvEngine.getFrameWidth(), cvEngine.getFrameHeight());
	}
	public void createEngines(){
		cvEngine = new CvEngineBase();
		glEngine = new GlEngineBase();		
	}
	public void sendMessage(MSG_ENUM receiver, String code, String message){
		if(receiver == MSG_ENUM.TO_CV && this.cvEngine != null){
				cvEngine.onMessage(code, message);
		}else if(receiver == MSG_ENUM.TO_GL && this.glEngine != null){
				glEngine.onMessage(code, message);
		}
	}
	
}
