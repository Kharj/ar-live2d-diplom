package com.kharj.ardiplom;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.utils.Converters;

import android.content.Context;
import android.util.Log;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.WindowManager;

public class CvEngineBase implements CvCameraViewListener2 {

	protected boolean isShowCamera = true;
	protected boolean flip = false;
	protected boolean isShowOverlay = true;
	protected BaseLoaderCallback loaderCallback;
	protected CameraBridgeViewBase cameraView;
	protected AREngineBase engine;
	protected Context appContext;
	protected CvProcessorBase processor;
	protected long fpsLastTime;
	protected long fpsFrames;
	protected int fpsValue;
	protected int cameraId;
	protected int frameWidth;
	protected int frameHeight;
	//public

	public boolean Init(Context context, AREngineBase _engine, CameraBridgeViewBase _cameraView){
		if(context == null || _cameraView == null) 
			return false;
		fpsLastTime = System.currentTimeMillis();
		fpsFrames = 0;
		this.engine = _engine;
		this.cameraView = _cameraView;
		this.appContext = context;
		this.loaderCallback = new BaseLoaderCallback(appContext) {
			@Override
			public void onManagerConnected(int status) {
				switch (status) {
				case LoaderCallbackInterface.SUCCESS:
				{
					Log.i(AREngineBase.TAG, "OpenCV loaded successfully");
					//Lib
					System.loadLibrary("ar-diplom-native");

					cameraView.enableView();
				} break;
				default:
				{
					super.onManagerConnected(status);
				} break;
				}
			}
		};
		if(loaderCallback == null) 
			return false;
		cameraView.setVisibility(SurfaceView.VISIBLE);
		cameraView.setCvCameraViewListener(this);
		//Processor
		createProcessor();
		if(processor != null)
			processor.Init(appContext);

		return true;
	}
	public void Destroy(){
		if (cameraView != null)
			cameraView.disableView();
		if(processor != null)
			processor.Destroy();
	}
	public void onResume(){
		OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_6, appContext, loaderCallback);
		if(processor != null)
			processor.onResume();
	}
	public void onPause(){
		if (cameraView != null)
			cameraView.disableView();
		if(processor != null)
			processor.onPause();
	}
	public boolean isShowOverlay() {
		return isShowOverlay;
	}
	public void setShowOverlay(boolean _isShowOverlay) {
		this.isShowOverlay = _isShowOverlay;
		if(processor != null){
			processor.setShowOverlay(isShowOverlay);
		}
	}
	public int getFPS() {
		return fpsValue;
	}
	public int getProcessorFPS() {
		return fpsValue;
	}
	public int getFrameWidth() {
		return frameWidth;
	}
	public int getFrameHeight() {
		return frameHeight;
	}
	@Override
	public void onCameraViewStarted(int width, int height) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCameraViewStopped() {
		// TODO Auto-generated method stub

	}

	@Override
	public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
		fpsWork();
		Mat source = getMatFromFrameWithRotation(inputFrame, false, flip);
		frameWidth = source.width();
		frameHeight = source.height();
		if(processor != null){
			processor.pushFrame(source);
			source = processor.pullFrame();
		}

		return source;
	}
	protected void fpsWork(){
		//FPS
		fpsFrames++;
		long currentTime = System.currentTimeMillis();
		if (currentTime - fpsLastTime >= 1000) {
			fpsValue = (int)((float)(fpsFrames*1000)/(currentTime - fpsLastTime) + 0.5f);
			fpsFrames = 0;
			fpsLastTime = currentTime;
		}

	}
	protected Mat getMatFromFrameWithRotation(CvCameraViewFrame inputFrame, boolean flipHorizontally, boolean flipVertically)
	{
		final int rotation = ((WindowManager) appContext.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getRotation();
		Mat source = inputFrame.rgba();
		int flipVal = (flipHorizontally)?1:-2;
		if((rotation == Surface.ROTATION_270 && !flipVertically) || (rotation != Surface.ROTATION_270 && flipVertically)){
			 flipVal = (flipHorizontally)?0:-1;
		}
		if(flipVal != -2){
			Core.flip(source, source, flipVal);
		}
		return source;
	}
	protected void createProcessor(){
		processor = new CvProcessorBase();
	}

	public void onMessage(String code, String message) {
		if(code=="cameraId"){
			this.cameraId = Integer.parseInt(message);
		}else if(code == "flip"){
			this.flip = Boolean.parseBoolean(message);
		}
	}
}
