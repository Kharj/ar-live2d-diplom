package com.kharj.ardiplom;

import java.util.NoSuchElementException;
import java.util.Scanner;

import android.content.Context;
import android.graphics.PixelFormat;
import android.opengl.GLSurfaceView;
import android.util.Log;

public class GlEngineLive2D extends GlEngineBase {

	@Override
	public boolean Init(Context _context, AREngineBase _engine, GLSurfaceView _surfaceView){

		if(_context == null || _surfaceView == null) 
			return false;
		this.appContext = _context;
		this.surfaceView = _surfaceView;
		this.engine = _engine;

		renderer = new GlRendererLive2D(appContext);
		if(renderer == null)
			return false;

		surfaceView.getHolder().setFormat(PixelFormat.TRANSLUCENT);
		surfaceView.setEGLContextClientVersion(1);//GLES 1.0
		surfaceView.setEGLConfigChooser( 8, 8, 8, 8, 16, 0 );
		surfaceView.setPreserveEGLContextOnPause(true);
		surfaceView.setRenderer(renderer);		
		return true;
	}
	@Override
	public void Destroy(){
		super.Destroy();
	}
	@Override
	public void onResume(){
		super.onResume();
	}
	@Override
	public void onPause(){
		super.onPause();
	}
	@Override
	public void onMessage(String code, String message) {
		if(code=="FaceData"){
			//float faceX = 0; 
			//float faceY = 0;
			try{
				Scanner scanner = new Scanner (message);
				scanner.next (); 
				float faceX = ((float)scanner.nextInt()/1000.0f)-0.5f; 
				float faceY = ((float)scanner.nextInt()/1000.0f)-0.5f; 
				int isSmile = scanner.nextInt();
				float angle = ((float)scanner.nextInt()/1000.0f); 
				final int faces = (scanner.nextInt()); 
				final int mode = (scanner.nextInt()); 
				
				final float translateX = ((mode==0)?faceX:0); 
				final float translateY = ((mode==0)?faceY:0); 
				final float lookX = ((mode==1)?(faceX*2.6f):0); 
				final float lookY = ((mode==1)?(faceY*-7.0f):0); 
				
				if(mode!=0){
					angle = lookY*Math.abs(lookX)*4.0f;
					if(lookX > 0.01f) angle = -angle;
				}
				final float faceAngle = angle; 
				final float eyeForm = ((mode==1)?((faces==0)?0.65f:0):0); 
				final float mouthForm = (isSmile==0) ? ((mode==1 && faces!=0)?0.5f:-0.5f) : 1.0f;
				final float mouthOpen = (isSmile==0) ? -0.5f : 0.42f;
				surfaceView.queueEvent(new Runnable() {
					public void run() {
						((GlRendererLive2D)renderer).setMouthParams(mouthForm, mouthOpen);
						if(faces!=0 || mode!=0){//in copy mode do not move to default place, if no faces
							((GlRendererLive2D)renderer).setFaceTranslate(translateX, translateY);
						}
						if(faces!= 0|| mode == 0){
							((GlRendererLive2D)renderer).setLook(lookX, lookY);
							((GlRendererLive2D)renderer).setHeadAngle(faceAngle);
						}
						((GlRendererLive2D)renderer).setEyeForm(eyeForm);
						if(Math.abs(faceAngle)>0.01f && mode==0) ((GlRendererLive2D)renderer).setHeadAngle(faceAngle);
					}});
			}catch(Exception ex){

			}
			//Log.i(AREngineBase.TAG, "MSG="+message+"  XY:"+faceX+" "+faceY);

		}
	}
}

