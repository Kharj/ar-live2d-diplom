package com.kharj.ardiplom;

import android.content.Context;

public class CvProcessorAsyncBase extends CvProcessorMTBase {

	public int tasksMaxNum = 3;
	protected int tasksRunning = 0;
	@Override
	public void Init(Context _context){
		super.Init(_context);
		if(tasks == null){
			tasks = new CvProcessorTaskBase[tasksMaxNum];
			for(int i=0;i<tasksMaxNum;i++){
				tasks[i] = new CvProcessorTaskAT();
			}
		}

	}

}
