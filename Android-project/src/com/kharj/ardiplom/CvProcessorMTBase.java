package com.kharj.ardiplom;

import org.opencv.core.Mat;

import android.content.Context;
import android.util.Log;

public class CvProcessorMTBase extends CvProcessorBase {
	public int tasksMaxNum = 3;
	CvProcessorTaskBase [] tasks = null;
	@Override
	public void Init(Context _context){
		super.Init(_context);
		if(tasks == null){
			tasks = new CvProcessorTaskBase[tasksMaxNum];
			for(int i=0;i<tasksMaxNum;i++){
				tasks[i] = new CvProcessorTaskBase();
			}
		}

	}
	public void Destroy(){
		if(tasks != null){
			for(CvProcessorTaskBase task : tasks){
				task.stop();
				tasks = null;
				System.gc();
			}
		}
		super.Destroy();
	}
	public int freeTasks(){
		int free = 0;
		for(CvProcessorTaskBase task : tasks){
			if(!task.isBusy()){
				free++;
			}
		}
		return free;
	}
	private CvProcessorTaskBase getOldestFinishedTask(){
		long minTime = Long.MAX_VALUE;
		CvProcessorTaskBase minTask = null;
		for(CvProcessorTaskBase task : tasks){
			if(!task.isBusy() && !task.isEmpty() && task.getStartTime() < minTime){
				minTask = task;
				minTime = task.getStartTime();
			}
		}
		return minTask;
	}
	public Mat getOldestFinishedFrame(){
		Mat result = null;
		CvProcessorTaskBase minTask = getOldestFinishedTask();
		if(minTask != null){
			result = minTask.frame;
			minTask.clean();
		}
		return result;
	}
	@Override
	public boolean pushFrame(Mat _frame){
		if(_frame == null) return false;
		CvProcessorTaskBase freeTask = null;
		for(CvProcessorTaskBase task : tasks){
			if(!task.isBusy() && task.isEmpty()){
				freeTask = task;
				break;
			}
		}
		if(freeTask==null){
			//if no free task, kill oldest
			freeTask = getOldestFinishedTask();
		}
		if(freeTask == null){
			//no free, no finished
			return false;
		}
		//ok

		Log.i(AREngineBase.TAG, "Starting task");
		return freeTask.start(_frame);

	}
	@Override
	public Mat pullFrame(){
		return getOldestFinishedFrame();	
	}
	@Override
	public void onResume() {
		super.onResume();
		Init(null);
	}
	@Override
	public void onPause() {
		super.onPause();
		Destroy();
	}

}
