#include <jni.h>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/objdetect/objdetect.hpp>
using namespace cv;
#include <string>
#include <vector>
#include <android/log.h>
using namespace std;
#define LOG_TAG "AREngine"
#define LOGD(...) ((void)__android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__))

extern "C" {
JNIEXPORT void JNICALL Java_com_kharj_ardiplom_CvProcessorBase_cvNativeProcessBase(JNIEnv*, jobject, jlong addrRgba);

JNIEXPORT void JNICALL Java_com_kharj_ardiplom_CvProcessorBase_cvNativeProcessBase(JNIEnv*, jobject, jlong addrRgba){
	Mat& mRgb = *(Mat*)addrRgba;
	rectangle(mRgb, Point(0, 0), Point(mRgb.size().width/2, mRgb.size().height/2), Scalar(255.0, 255.0, 0.0), 25);
	//GaussianBlur( mRgb, mRgb, Size(3, 3), 2);
}

//com.kharj.ardiplom.CvProcessorFaceDetection

JNIEXPORT jlong JNICALL Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeCreateObject
(JNIEnv *, jclass, jstring, jint);

JNIEXPORT void JNICALL Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDestroyObject
(JNIEnv *, jclass, jlong);

JNIEXPORT void JNICALL Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDetect
(JNIEnv *, jclass, jlong, jlong, jlong, jint, jint);


JNIEXPORT void JNICALL Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDetectFaceSmile
(JNIEnv *, jclass, jlong, jlong, jlong, jlong, jlong, jlong, jlong, jint, jint);

inline void vector_Rect_to_Mat(vector<Rect>& v_rect, Mat& mat)
{
	mat = Mat(v_rect, true);
}

JNIEXPORT jlong JNICALL Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeCreateObject
(JNIEnv * jenv, jclass, jstring jFileName, jint faceSize)
{
	LOGD("Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeCreateObject enter");
	const char* jnamestr = jenv->GetStringUTFChars(jFileName, NULL);
	string stdFileName(jnamestr);
	jlong result = 0;

	try
	{
		result = (jlong)new CascadeClassifier(stdFileName);
	}
	catch(cv::Exception& e)
	{
		LOGD("nativeCreateObject caught cv::Exception: %s", e.what());
		jclass je = jenv->FindClass("org/opencv/core/CvException");
		if(!je)
			je = jenv->FindClass("java/lang/Exception");
		jenv->ThrowNew(je, e.what());
	}
	catch (...)
	{
		LOGD("nativeCreateObject caught unknown exception");
		jclass je = jenv->FindClass("java/lang/Exception");
		jenv->ThrowNew(je, "Unknown exception in JNI code of nativeCreateObject()");
		return 0;
	}

	LOGD("Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeCreateObject exit");
	return result;
}

JNIEXPORT void JNICALL Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDestroyObject
(JNIEnv * jenv, jclass, jlong thiz)
{
	LOGD("Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDestroyObject enter");
	try
	{
		if(thiz != 0)
		{
			delete (CascadeClassifier*)thiz;
		}
	}
	catch(cv::Exception& e)
	{
		LOGD("nativeestroyObject caught cv::Exception: %s", e.what());
		jclass je = jenv->FindClass("org/opencv/core/CvException");
		if(!je)
			je = jenv->FindClass("java/lang/Exception");
		jenv->ThrowNew(je, e.what());
	}
	catch (...)
	{
		LOGD("nativeDestroyObject caught unknown exception");
		jclass je = jenv->FindClass("java/lang/Exception");
		jenv->ThrowNew(je, "Unknown exception in JNI code of nativeDestroyObject()");
	}
	LOGD("Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDestroyObject exit");
}

JNIEXPORT void JNICALL Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDetect
(JNIEnv * jenv, jclass, jlong thiz, jlong imageGray, jlong faces, jint minSizeX, jint minSizeY)
{
	//LOGD("Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDetect enter");
	try
	{
		if(thiz){
			vector<Rect> RectFaces;
			((CascadeClassifier*)thiz)->detectMultiScale(*((Mat*)imageGray), RectFaces, 1.25f, 3, CASCADE_FIND_BIGGEST_OBJECT | CASCADE_SCALE_IMAGE, Size(minSizeX, minSizeY));
			//((DetectionBasedTracker*)thiz)->getObjects(RectFaces);
			vector_Rect_to_Mat(RectFaces, *((Mat*)faces));

		}
	}
	catch(cv::Exception& e)
	{
		LOGD("nativeCreateObject caught cv::Exception: %s", e.what());
		jclass je = jenv->FindClass("org/opencv/core/CvException");
		if(!je)
			je = jenv->FindClass("java/lang/Exception");
		jenv->ThrowNew(je, e.what());
	}
	catch (...)
	{
		LOGD("nativeDetect caught unknown exception");
		jclass je = jenv->FindClass("java/lang/Exception");
		jenv->ThrowNew(je, "Unknown exception in JNI code DetectionBasedTracker.nativeDetect()");
	}
	//LOGD("Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDetect exit");
}

JNIEXPORT void JNICALL Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDetectFaceSmile
(JNIEnv * jenv, jclass, jlong thiz, jlong thiz2, jlong thiz3, jlong imageGray, jlong faces, jlong smiles, jlong eyes, jint minSizeX, jint minSizeY)
{
	//LOGD("Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDetect enter");
	try
	{
		if(thiz && thiz2){
			vector<Rect> RectFaces;
			vector<Rect> RectSmiles;
			vector<Rect> RectEyes;
			((CascadeClassifier*)thiz)->detectMultiScale(*((Mat*)imageGray), RectFaces, 1.25f, 3, CASCADE_FIND_BIGGEST_OBJECT | CASCADE_SCALE_IMAGE, Size(minSizeX, minSizeY));
			for(int i=0;i<RectFaces.size();++i){
				Rect roi = RectFaces[i];
				Rect roiEyes = roi;
				int halfRoiHeight = roi.height/2;
				roiEyes.height = halfRoiHeight;
				Rect roiSmile = roi;
				roiSmile.height = halfRoiHeight-1;
				roiSmile.y += halfRoiHeight;
				Mat smileImg = (*((Mat*)imageGray))(roiSmile);
				Mat eyesImg = (*((Mat*)imageGray))(roiEyes);
				//Smile
				((CascadeClassifier*)thiz2)->detectMultiScale(smileImg, RectSmiles, 2.5f, 3, CASCADE_FIND_BIGGEST_OBJECT | CASCADE_SCALE_IMAGE, Size(minSizeX/2, minSizeY/6));
				for(vector<Rect>::iterator j = RectSmiles.begin(); j!= RectSmiles.end(); ){
					/*if((*j).y < roiSmile.height*0.5f){
						//delete rect, if it is not in botton half of ROI
						RectSmiles.erase(j);
					}else{*/
						(*j).x += roiSmile.x;
						(*j).y += roiSmile.y;

						j++;
					//}
				}
				//Eyes
				if(thiz3 != 0){
					((CascadeClassifier*)thiz3)->detectMultiScale(eyesImg, RectEyes, 1.2f, 2, CASCADE_SCALE_IMAGE, Size(minSizeX/10, minSizeY/10));
					for(vector<Rect>::iterator j = RectEyes.begin(); j!= RectEyes.end(); ){
						(*j).x += roiEyes.x;
						(*j).y += roiEyes.y;
						j++;
					}
				}
			}
			vector_Rect_to_Mat(RectSmiles, *((Mat*)smiles));
			vector_Rect_to_Mat(RectFaces, *((Mat*)faces));
			vector_Rect_to_Mat(RectEyes, *((Mat*)eyes));

		}
	}
	catch(cv::Exception& e)
	{
		LOGD("nativeCreateObject caught cv::Exception: %s", e.what());
		jclass je = jenv->FindClass("org/opencv/core/CvException");
		if(!je)
			je = jenv->FindClass("java/lang/Exception");
		jenv->ThrowNew(je, e.what());
	}
	catch (...)
	{
		LOGD("nativeDetect caught unknown exception");
		jclass je = jenv->FindClass("java/lang/Exception");
		jenv->ThrowNew(je, "Unknown exception in JNI code DetectionBasedTracker.nativeDetect()");
	}
	//LOGD("Java_com_kharj_ardiplom_CvProcessorFaceDetection_nativeDetect exit");
}

}

