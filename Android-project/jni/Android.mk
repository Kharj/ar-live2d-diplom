LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)
include ../OpenCV-2.4.9-android-sdk/native/jni/OpenCV.mk

LOCAL_MODULE    := ar-diplom-native
LOCAL_SRC_FILES := ar-diplom-native.cpp
LOCAL_LDLIBS    += -llog -ldl

include $(BUILD_SHARED_LIBRARY)

$(call import-module,android/native_app_glue)